<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;

class PointController extends ActiveController
{
    public $modelClass = \app\models\Points::class;
}