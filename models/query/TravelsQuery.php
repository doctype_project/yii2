<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Travels]].
 *
 * @see \app\models\Travels
 */
class TravelsQuery extends \yii\db\ActiveQuery
{
    /**
     * @param integer $userId User->id
     * @return $this
     */
    public function withUser($userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Travels[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Travels|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
