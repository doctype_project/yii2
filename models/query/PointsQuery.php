<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Points]].
 *
 * @see \app\models\Points
 */
class PointsQuery extends \yii\db\ActiveQuery
{
    /**
     * @param integer $travelId Travels->id
     * @return $this
     */
    public function withTravel($travelId)
    {
        return $this->andWhere(['travel_id' => $travelId]);
    }

    /**
     * @param integer $vehicle_id Vehicle->id
     * @return $this
     */
    public function withVehicle($vehicle_id)
    {
        return$this->andWhere(['vehicle_id' => $vehicle_id]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Points[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Points|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
