<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Comments]].
 *
 * @see \app\models\Comments
 */
class CommentsQuery extends \yii\db\ActiveQuery
{
    /**
     * @param integer $userId User->id
     * @return $this
     */
    public function withUser($userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    /**
     * @param integer $travelId Travels->id
     * @return $this
     */
    public function withTravel($travelId)
    {
        return $this->andWhere(['travel_id' => $travelId]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Comments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Comments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
