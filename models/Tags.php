<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $title
 *
 * @property TagsTravel[] $tagsTravels
 * @property Travels[] $travels
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsTravels()
    {
        return $this->hasMany(TagsTravel::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravels()
    {
        return $this->hasMany(Travels::className(), ['id' => 'travel_id'])->viaTable('tags_travel', ['tag_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\TagsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TagsQuery(get_called_class());
    }
}
