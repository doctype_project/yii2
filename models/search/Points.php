<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Points as PointsModel;

/**
 * Points represents the model behind the search form of `app\models\Points`.
 */
class Points extends PointsModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'travel_id', 'vehicle_id', 'number'], 'integer'],
            [['coordinates', 'title', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'travel_id' => $this->travel_id,
            'vehicle_id' => $this->vehicle_id,
            'number' => $this->number,
        ]);

        $query->andFilterWhere(['like', 'coordinates', $this->coordinates])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
