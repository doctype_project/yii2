<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%photo}}".
 *
 * @property integer $id
 * @property integer $travel_id
 * @property string $title
 * @property string $filename
 *
 * @property Travels $travel
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['travel_id'], 'required'],
            [['travel_id'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['filename'], 'string', 'max' => 255],
            [['travel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Travels::className(), 'targetAttribute' => ['travel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'travel_id' => Yii::t('app', 'Travel ID'),
            'title' => Yii::t('app', 'Title'),
            'filename' => Yii::t('app', 'Filename'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravel()
    {
        return $this->hasOne(Travels::className(), ['id' => 'travel_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\PhotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PhotoQuery(get_called_class());
    }
}
