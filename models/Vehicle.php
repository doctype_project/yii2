<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%vehicle}}".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Points[] $points
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vehicle}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasMany(Points::className(), ['vehicle_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\VehicleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\VehicleQuery(get_called_class());
    }
}
