<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AddTravelForm is the model behind the Add Travel Form.
 *
 */
class AddTravelForm extends Model
{
    public $vehicle = array();
    public $address = array();
    public $day = array();
    public $month = array();
    public $year = array();
    public $travelTitle;
    public $travelDescription;
    public $tag = array();


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['vehicle', 'address', 'day', 'month', 'year', 'travelTitle', 'travelDescription', 'tag'], 'safe']
        ];
    }

    /**
     * @return bool|null|integer - id last added Travel
     */
    public function addTravel()
    {
        if (!$this->validate()) {
            return null;
        }
        $travel = new Travels();
        $user_id = Yii::$app->user->id;
        $travel->user_id = $user_id;
        $travel->title = $this->travelTitle;
        $travel->description = $this->travelDescription;
        $travel->date_begin = strtotime($this->year[0] . '-' . $this->month[0] . '-' . $this->day[0]);
        $travel->date_end = strtotime($this->year[1] . '-' . $this->month[1] . '-' . $this->day[1]);

        if ($travel->save()) {
            return $travel->primaryKey;
        }
        return false;
    }

    /**
     * @param $travel_id
     * @return bool
     */
    public function addPoint($travel_id)
    {
        foreach ($this->address as $key => $value)
        {
            $point = new Points();
            $point->travel_id = $travel_id;
            $point->vehicle_id = (int)$this->vehicle[$key];
            $point->number = (int)$key;
            $point->coordinates = $value;
            if (!$point->save()) {
                var_dump($point->errors);
                die('point');
                return false;
            }
        }
        return true;
    }

    /**
     * @param $travel_id
     * @return bool
     */
    public function addTag($travel_id)
    {
        foreach ($this->tag as $key => $value) {
            if ($value) {
                $tag = new TagsTravel();
                $tag->travel_id = $travel_id;
                $tag->tag_id = $value;
                if (!$tag->save()) {
                    var_dump($tag->errors);
                    die('tag');
                    return false;
                }
            }
        }
        return true;
    }
}
