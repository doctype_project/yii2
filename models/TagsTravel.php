<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tags_travel}}".
 *
 * @property integer $tag_id
 * @property integer $travel_id
 *
 * @property Tags $tag
 * @property Travels $travel
 */
class TagsTravel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tags_travel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'travel_id'], 'required'],
            [['tag_id', 'travel_id'], 'integer'],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tag_id' => 'id']],
            [['travel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Travels::className(), 'targetAttribute' => ['travel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => Yii::t('app', 'Tag ID'),
            'travel_id' => Yii::t('app', 'Travel ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravel()
    {
        return $this->hasOne(Travels::className(), ['id' => 'travel_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\TagsTravelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TagsTravelQuery(get_called_class());
    }
}
