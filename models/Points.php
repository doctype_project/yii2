<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%points}}".
 *
 * @property integer $id
 * @property integer $travel_id
 * @property integer $vehicle_id
 * @property integer $number
 * @property string $coordinates
 * @property string $title
 * @property string $description
 *
 * @property Travels $travel
 * @property Vehicle $vehicle
 */
class Points extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%points}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['travel_id', 'vehicle_id', 'number', 'coordinates'], 'required'],
            [['travel_id', 'vehicle_id', 'number'], 'integer'],
            [['description'], 'string'],
            [['coordinates'], 'string', 'max' => 23],
            [['title'], 'string', 'max' => 45],
            [['travel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Travels::className(), 'targetAttribute' => ['travel_id' => 'id']],
            [['vehicle_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::className(), 'targetAttribute' => ['vehicle_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'travel_id' => Yii::t('app', 'Travel ID'),
            'vehicle_id' => Yii::t('app', 'Vehicle ID'),
            'number' => Yii::t('app', 'Number'),
            'coordinates' => Yii::t('app', 'Coordinates'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravel()
    {
        return $this->hasOne(Travels::className(), ['id' => 'travel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(Vehicle::className(), ['id' => 'vehicle_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\PointsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PointsQuery(get_called_class());
    }
}
