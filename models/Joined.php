<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%joined}}".
 *
 * @property integer $user_id
 * @property integer $travel_id
 *
 * @property Travels $travel
 * @property User $user
 */
class Joined extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%joined}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'travel_id'], 'required'],
            [['user_id', 'travel_id'], 'integer'],
            [['travel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Travels::className(), 'targetAttribute' => ['travel_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'travel_id' => Yii::t('app', 'Travel ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravel()
    {
        return $this->hasOne(Travels::className(), ['id' => 'travel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\JoinedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\JoinedQuery(get_called_class());
    }
}
