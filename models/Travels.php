<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%travels}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property integer $liked
 * @property integer $joined
 * @property string $description
 * @property string $date_create
 * @property string $date_begin
 * @property string $date_end
 *
 * @property Comments[] $comments
 * @property Joined[] $joineds
 * @property User[] $joinedUsers
 * @property Liked[] $likeds
 * @property User[] $LikedUsers
 * @property Photo[] $photos
 * @property Points[] $points
 * @property TagsTravel[] $tagsTravels
 * @property Tags[] $tags
 * @property User $user
 */
class Travels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%travels}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'date_begin', 'date_end'], 'required'],
            [['user_id', 'liked', 'joined'], 'integer'],
            [['description'], 'string'],
            [['date_create', 'date_begin', 'date_end'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'liked' => Yii::t('app', 'Liked'),
            'joined' => Yii::t('app', 'Joined'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_begin' => Yii::t('app', 'Date Begin'),
            'date_end' => Yii::t('app', 'Date End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoineds()
    {
        return $this->hasMany(Joined::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinedUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%joined}}', ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeds()
    {
        return $this->hasMany(Liked::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikedUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%liked}}', ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasMany(Points::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsTravels()
    {
        return $this->hasMany(TagsTravel::className(), ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['id' => 'tag_id'])->viaTable('{{%tags_travel}}', ['travel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\TravelsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TravelsQuery(get_called_class());
    }
}
