<?php

use yii\db\Migration;

class m161116_094019_photo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'travel_id' => $this->integer()->notNull(),
            'title' => $this->string(45),
            'filename' => $this->string(255),
        ], $tableOptions);

        $this->createIndex('photo_travel', '{{%photo}}', 'travel_id');

        $this->addForeignKey('FK_photo_travel', '{{%photo}}', 'travel_id', '{{%travels}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_photo_travel', '{{%photo}}');
        $this->dropTable('{{%photo}}');
    }
}
