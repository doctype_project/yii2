<?php

use yii\db\Migration;

class m161116_095854_points extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%points}}', [
            'id' => $this->primaryKey(),
            'travel_id' => $this->integer()->notNull(),
            'vehicle_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'coordinates' => $this->string(23)->notNull(),
            'title' => $this->string(45),
            'description' => $this->text(),
        ], $tableOptions);

        $this->createIndex('points_travel', '{{%points}}', 'travel_id');
        $this->createIndex('points_vehicle', '{{%points}}', 'vehicle_id');

        $this->addForeignKey('FK_points_travel', '{{%points}}', 'travel_id', '{{%travels}}', 'id');
        $this->addForeignKey('FK_points_vehicle', '{{%points}}', 'vehicle_id', '{{%vehicle}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_points_travel', '{{%points}}');
        $this->dropForeignKey('FK_points_vehicle', '{{%points}}');

        $this->dropTable('{{%points}}');
    }
}
