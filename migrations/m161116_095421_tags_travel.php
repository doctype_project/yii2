<?php

use yii\db\Migration;

class m161116_095421_tags_travel extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tags_travel}}', [
            'tag_id' => $this->integer()->notNull(),
            'travel_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PK_tags_travel', '{{%tags_travel}}', ['travel_id', 'tag_id']);

        $this->createIndex('tags_travel_tag', '{{%tags_travel}}', 'tag_id');
        $this->createIndex('tags_travel_travel', '{{%tags_travel}}', 'travel_id');

        $this->addForeignKey('FK_tags_travel_tag', '{{%tags_travel}}', 'tag_id', '{{%tags}}', 'id');
        $this->addForeignKey('FK_tags_travel_travel', '{{%tags_travel}}', 'travel_id', '{{%travels}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_tags_travel_tag', '{{%tags_travel}}');
        $this->dropForeignKey('FK_tags_travel_travel', '{{%tags_travel}}');
        $this->dropTable('{{%tags_travel}}');
    }
}
