<?php

use yii\db\Migration;

class m161116_093245_liked extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%liked}}', [
            'user_id' => $this->integer()->notNull(),
            'travel_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PK_liked', '{{%liked}}', ['user_id', 'travel_id']);

        $this->createIndex('liked_user', '{{%liked}}', 'user_id');
        $this->createIndex('liked_travel', '{{%liked}}', 'travel_id');

        $this->addForeignKey('FK_liked_user', '{{%liked}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_liked_travel', '{{%liked}}', 'travel_id', '{{%travels}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_liked_user', '{{%liked}}');
        $this->dropForeignKey('FK_liked_travel', '{{%liked}}');
        $this->dropTable('{{%liked}}');
    }
}
