<?php

use yii\db\Migration;

class m161116_090805_travels extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%travels}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string(45)->notNull(),
            'liked' => $this->integer()->defaultValue(0),
            'joined' => $this->integer()->defaultValue(0),
            'description' => $this->text(),
            'date_create' => $this->timestamp()->notNull(),
            'date_begin' => $this->integer()->notNull(),
            'date_end' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('travels_user', '{{%travels}}', 'user_id');
        $this->addForeignKey('FK_travels_user', '{{%travels}}', 'user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_travels_user', '{{%travels}}');
        $this->dropTable('{{%travels}}');
    }
}
