<?php

use yii\db\Migration;

class m161116_093646_joined extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%joined}}', [
            'user_id' => $this->integer()->notNull(),
            'travel_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PK_joined', '{{%joined}}', ['user_id', 'travel_id']);

        $this->createIndex('joined_user', '{{%joined}}', 'user_id');
        $this->createIndex('joined_travel', '{{%joined}}', 'travel_id');

        $this->addForeignKey('FK_joined_user', '{{%joined}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_joined_travel', '{{%joined}}', 'travel_id', '{{%travels}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_joined_user', '{{%joined}}');
        $this->dropForeignKey('FK_joined_travel', '{{%joined}}');
        $this->dropTable('{{%joined}}');
    }
}
