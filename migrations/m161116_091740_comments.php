<?php

use yii\db\Migration;

class m161116_091740_comments extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'travel_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->createIndex('comments_user', '{{%comments}}', 'user_id');
        $this->createIndex('comments_travel', '{{%comments}}', 'travel_id');

        $this->addForeignKey('FK_comments_user', '{{%comments}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_comments_travel', '{{%comments}}', 'travel_id', '{{%travels}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_comments_user', '{{%comments}}');
        $this->dropForeignKey('FK_comments_travel', '{{%comments}}');
        $this->dropTable('{{%comments}}');
    }

}
