Веб приложение Walkbike - основано на Yii 2 Advanced Project Template
===============================
Краткая информация
-------------------

Любителям пеших и вело путешествий посвящается. Можно описать маршрут, даты, предполагаемое время путешествия. Любой может присоединиться.

Файловая структура
-------------------

```
    composer.json       используется Composer'ом, содержит описание приложения
    config/             конфигурационные файлы
        console.php     конфигурация консольного приложения
        web.php         конфигурация Web приложения
    commands/           содержит классы консольных команд
    controllers/        контроллеры
    models/             модели
    runtime/            файлы, которые генерирует Yii во время выполнения приложения (логи, кэш и т.п.)
    vendor/             содержит пакеты Composer'а и, собственно, сам фреймворк Yii
    views/              виды приложения
    web/                корневая директория Web приложения. Содержит файлы, доступные через Web
        assets/         скрипты, используемые приложением (js, css)
        index.php       точка входа в приложение Yii. С него начинается выполнение приложения
    yii                 скрипт выполнения консольного приложения Yii
```
# Требования #
Минимальное требование по yii-это поддержка Вашим Веб-сервером PHP 5.4

# Установка приложения: #
* Получение фалов из репозитория (git clone или распаковка zip архива)
* [Глобальная установка composer](https://getcomposer.org/doc/00-intro.md#globally)
* Выполните следующие команды в консоли  `composer global require "fxp/composer-asset-plugin:^1.2.0"`
`composer install`

# Подготовка приложения #
* Создайте БД для приложения
* Отредактируйте файл настройки соединения с БД  `/config/db.php`
* Применить миграции, выполнив в консоли команду  `php yii migrate`


# Описание сервисов REST API #

## Для получения ответа в формате json требуется передать заголовок "accept: application/json", иначе данные будут возвращаться в формате xml ##

## В веб приложении реализованы следующие методы: ##
* GET api/point: Получение списка маркеров для карты;
* POST api/point: Создать новый маркер;
* GET api/point/123: Получить маркер с id 123;
* PATCH api/point/123 and PUT api/point/123: Изменить маркер с id 123;
* DELETE api/point/123: удалить маркер с id 123;

## Метод GET Получение списка маркеров для карты ##
### адрес: 
api/point
### пример ответа: 
[{"id":1,"travel_id":1,"vehicle_id":1,"number":324,"coordinates":"234","title":"234243","description":"34"},{"id":2,"travel_id":1,"vehicle_id":1,"number":45354,"coordinates":"3365546","title":"345456","description":"35445"},{"id":3,"travel_id":1,"vehicle_id":1,"number":4,"coordinates":"34.3423 56.2345","title":"Точка 1","description":"Описаниеточки 1"}]

## Метод POST Создать новый маркер ##
### адрес: 
api/point
### запрос: 
Content-type: application/x-www-form-urlencoded

Raw payload: travel_id=1&vehicle_id=1&number=4&coordinates=34.3423 56.2345&title=Точка 1&description=Описаниеточки 1
### пример ответа: 
{
"travel_id": "1",
"vehicle_id": "1",
"number": "4",
"coordinates": "34.3423 56.2345",
"title": "Точка 1",
"description": "Описаниеточки 1",
"id": 4
}

*Статус 201: created*

## Метод GET Получить маркер ##
### адрес: 
api/point/N - где N = id маркера
### пример ответа: 
{
"id": 4,
"travel_id": 1,
"vehicle_id": 1,
"number": 4,
"coordinates": "34.3423 56.2345",
"title": "Точка 1",
"description": "Описаниеточки 1"
}

## Метод PUT или PATCH Изменить маркер ##
### адрес: 
api/point/N - где N = id маркера
### запрос: 
Content-type: application/x-www-form-urlencoded

Raw payload: travel_id=1&vehicle_id=1&number=4&coordinates=34.3423 56.2345&title=Точка 1&description=Описаниеточки 1
### пример ответа: 
{
"id": 4,
"travel_id": "1",
"vehicle_id": "1",
"number": "4",
"coordinates": "34.3423 56.2345",
"title": "Точка 1",
"description": "Описаниеточки 1"
}

*Статус: 200 ok*

### пример ответа, если маркер с данным id не найден ###
{
"name": "Not Found",
"message": "Object not found: 4",
"code": 0,
"status": 404,
"type": "yii\web\NotFoundHttpException"
}

*Статус: 404 not found*

## Метод DELETE Удалить маркер ##
### адрес: 
api/point/N - где N = id маркера
### пример ответа: 
*Пустой ответ статус 204 no content*

### пример ответа, если маркер с данным id не найден ###
{
"name": "Not Found",
"message": "Object not found: 4",
"code": 0,
"status": 404,
"type": "yii\web\NotFoundHttpException"
}

*Статус: 404 not found*