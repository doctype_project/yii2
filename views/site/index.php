<?php

/* @var $this yii\web\View */
/* @var $tags \app\models\Tags[] */
/* @var $vehicles \app\models\Vehicle[] */
/* @var $addTravelForm \app\models\AddTravelForm */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\ListView;

$this->title = 'Home page';
?>
<h2>Создайте свое путешествие</h2>

<?php
$form = ActiveForm::begin([
    'id' => 'AddTravel',
    'options' => [
        'class' => 'row form'
    ],
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ],
    ],
]);
$days = [1 => 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
$months = [1 => 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
$years = [2016 => 2016, 2017, 2018, 2019];
?>

    <div class="col form__map" id="mapCanvas"></div>

    <section class="col form__input">
        <div class="row input-block pointinput">
            <div class="col address_block">
                <?= $form->field($addTravelForm, 'address[]')->textInput(['class' => 'input-block_style __mod', 'placeholder' => 'Адрес или точка на карте'])->label(false) ?>
                <div class="abc red"><span>a</span></div>
            </div>
            <div class="col-r">
                <?= $form->field($addTravelForm, 'vehicle[]')->dropDownList($vehicles, [
                    'class' => 'input-block_style starttransport',
                    'prompt' => 'Выберите транспорт'
                ])->label(false) ?>
            </div>
        </div>
        <div class="row input-block">
            <div class="col">
                <span>Выезд:</span>
                <?= $form->field($addTravelForm, 'day[]', [
                    'template' => '{input}',
                ])->dropDownList($days)->label(false) ?>
                <?= $form->field($addTravelForm, 'month[]', [
                    'template' => '{input}',
                ])->dropDownList($months)->label(false) ?>
                <?= $form->field($addTravelForm, 'year[]', [
                    'template' => '{input}',
                ])->dropDownList($years)->label(false) ?>
            </div>
            <div class="col-r">
                <span>Приезд:</span>
                <?= $form->field($addTravelForm, 'day[]', [
                    'template' => '{input}',
                ])->dropDownList($days)->label(false) ?>
                <?= $form->field($addTravelForm, 'month[]', [
                    'template' => '{input}',
                ])->dropDownList($months)->label(false) ?>
                <?= $form->field($addTravelForm, 'year[]', [
                    'template' => '{input}',
                ])->dropDownList($years)->label(false) ?>
            </div>
        </div>
        <div class="row input-block pointinput">
            <div class="col address_block">
                <?= $form->field($addTravelForm, 'address[]')->textInput(['class' => 'input-block_style __mod', 'placeholder' => 'Адрес или точка на карте'])->label(false) ?>
                <div class="abc green"><span>b</span></div>
            </div>
            <div class="col-r">
                <?= $form->field($addTravelForm, 'vehicle[]')->dropDownList($vehicles, [
                    'class' => 'input-block_style starttransport',
                    'prompt' => 'Выберите транспорт'
                ])->label(false) ?>
            </div>
        </div>

        <div class="row add-reset-block __mod">
            <div class="col input-block_style">
                <div class="col">
                    <a class="item__btn" onclick="createInputBlock();">Добавить точку</a>
                </div>
                <div class="col-r">
                    <a class="item__btn" onclick="removeInputBlock();">Удалить точку</a>
                </div>
            </div>
            <div class="col-r input-block_style">
                <div class="col-r">
                    <a class="item__btn" onclick="deleteMarkers();">Сбросить</a>
                </div>
            </div>
        </div>

        <div class="row input-block">
            <div class="col">
                <?= $form->field($addTravelForm, 'travelTitle')->textInput(['class' => 'input-block_style', 'placeholder' => 'Название поездки'])->label(false) ?>
            </div>
            <div class="col-r">
                <?= $form->field($addTravelForm, 'tag[]')->dropDownList($tags, [
                    'class' => 'input-block_style',
                    'prompt' => 'Добавить тег'
                ])->label(false) ?>
            </div>
        </div>
        <div class="row input-block">
            <div class="col">
                <?= $form->field($addTravelForm, 'travelDescription')->textInput(['class' => 'input-block_style', 'placeholder' => 'Краткое описание'])->label(false) ?>
            </div>
            <div class="col-r">
                <?= $form->field($addTravelForm, 'tag[]')->dropDownList($tags, [
                    'class' => 'input-block_style',
                    'prompt' => 'Добавить тег'
                ])->label(false) ?>
            </div>
        </div>

        <?php
        if (!Yii::$app->user->isGuest):
        ?>
        <div class="row input-block">
            <div class="col">
                <?= Html::buttonInput('Предварительный просмотр', ['class' => 'input-block__btn']) ?>
            </div>
            <div class="col-r">
                <?= Html::submitInput('Создать путешествие', ['class' => 'input-block__btn']) ?>
            </div>
        </div>
        <?php
        else:
        ?>
        <div class="row form__items">
            <span>Чтобы создать путешествие Вам необходимо <a href="" class="items__btn">войти</a> или <a href="" class="items__btn">зарегистрироваться</a></span>
        </div>
        <?php
        endif;
        ?>
    </section>
<?php ActiveForm::end()?>

<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'travel',
]);
?>

<br>

<section class="row view__more">
    <a role="button" class="more__btn">
        <span class="btn__text">посмотреть ещё</span>
    </a>
</section>
