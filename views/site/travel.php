<?php
/* @var $model \app\models\Travels */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<br>
<div>
    <section class="row hot-offer">
        <header class="row hot-offer__header">
            <h4><?= Html::encode($model->title) ?></h4>
            <p><?= Html::encode($model->description) ?></p>
        </header>
        <div class="row hot-offer__block">
            <img src="" alt="" class="col main-added__img">
            <div class="col calendar">календарь</div>
            <div class="col hot-offer__organizer">
                <h4>Организатор:</h4>
                <div class="row">
                    <img src="" class="col organizer__author" alt="ава">
                    <div class="col organizer__inform">
                        <h4 class="inform__username"><?= Html::encode($model->user->username) ?></h4>
                        <p class="inform__p">мужчина</p>
                        <p class="inform__p">23 года</p>
                        <p class="inform__p">г. Москва</p>
                        <p class="inform__p __gray">3 поездки</p>
                    </div>
                </div>
                <div class="row organizer__soc-site">
                    <div class="col">
                        <a href="https://vk.com/" target="_blank" class="soc-site__sprites"></a>
                    </div>
                    <div class="col">
                        <a href="https://www.facebook.com" target="_blank" class="soc-site__sprites __facebook"></a>
                    </div>
                    <div class="col">
                        <a href="https://twitter.com" target="_blank" class="soc-site__sprites __twitter"></a>
                    </div>
                </div>
            </div>
            <div class="col-r tags-block">
                <?php
                foreach ($model->tags as $tag):
                ?>
                <div class="row">
                    <span class="tags-block__tag"><?= Html::encode($tag->title) ?></span>
                </div>
                <?php
                endforeach;
                ?>
            </div>
        </div>

        <footer class="row block__icon-btn">
            <ul class="flex-container">
                <li class="col icon-btn__block">
                    <a href="#" class="block__btn">
                        <div class="col btn__icon-sprites __like"></div>
                        <div class="btn__label">
                            <span class="label-hover __count">0</span>
                            <span class="label-hover">человек оценило</span>
                        </div>
                    </a>
                </li>
                <li class="col icon-btn__block">
                    <a href="#" class="block__btn">
                        <div class="col btn__icon-sprites __map"></div>
                        <div class="btn__label">
                            <span class="label-hover">Маршрут</span>
                        </div>
                    </a>
                </li>
                <li class="col icon-btn__block">
                    <a href="#" class="block__btn">
                        <div class="col btn__icon-sprites __users-checked"></div>
                        <div class="btn__label">
                            <span class="label-hover __count">0</span>
                            <span class="label-hover">человек присоединилось</span>
                        </div>
                    </a>
                </li>
                <li class="col icon-btn__block">
                    <a href="#" class="block__btn">
                        <div class="col btn__icon-sprites __checked"></div>
                        <div class="btn__label">
                            <span class="label-hover">Присоединиться</span>
                        </div>
                    </a>
                </li>
            </ul>
        </footer>
    </section>
</div>