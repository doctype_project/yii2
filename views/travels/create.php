<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Travels */

$this->title = 'Create Travels';
$this->params['breadcrumbs'][] = ['label' => 'Travels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="travels-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
