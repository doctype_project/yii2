<?php

/* @var $this \yii\web\View */
/* @var $content string */

use Yii;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\WalkbikeAsset;
use app\widgets\Alert;
use yii\helpers\Url;
WalkbikeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<section class="main-wrap">
    <section class="flexbox">

        <header class="row main-header">
            <div class="col logo-block">
                <a href="<?= Url::to(['site/index']) ?>" role="banner">
                    <div class="col logo-block__logo"></div>
                    <div class="col logo-block__walkbike">
                        <h1>
                            <span class="walkbike-blue">W</span>alk<span class="walkbike-blue">B</span>ike
                        </h1>
                    </div>
                </a>
            </div>
            <div class="col slogan-block">
                <h1>
                    <span>Две</span> ноги<span>!</span> <span>Два</span> колеса<span>!</span>
                </h1>
            </div>
            <div class="col-r login-block">
                <?php
                if (Yii::$app->user->isGuest):
                    ?>
                    <div class="col login-block__btn">
                        <a href="<?= Url::to(['site/login']) ?>">Войти</a>
                    </div>
                    <div class="col login-block__btn">
                        <a href="<?= Url::to(['site/signup']) ?>" class="login-block__btn __mod">Зарегистрироваться</a>
                    </div>
                    <?php
                else:
                    ?>
                    <div class="col login-block__btn">
                        <a href="#"><?= Html::encode(Yii::$app->user->identity->username) ?></a>
                    </div>
                    <?= Html::a('Выйти', Url::to(['site/logout']), ['class' => 'col login-block__btn __mod','data-method' => 'POST']) ?>
                    <?php
                endif;
                ?>
            </div>
        </header>
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="info">
                <?php echo Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::$app->session->hasFlash('error')):?>
            <div class="info">
                <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <?= $content ?>

    <footer class="row main-footer">
        <nav class="main-footer__nav">
            <a href="<?= Url::to(['site/index']) ?>" class="main-footer__item">Главная</a>
            <a href="<?= Url::to(['site/about']) ?>" class="main-footer__item">О нас</a>
            <a href="#" class="main-footer__item">Партненрство</a>
            <a href="#" class="main-footer__item">Правила использования</a>
            <a href="#" class="main-footer__item">Конфиденциальность</a>
        </nav>
    </footer>

</section><!--main-wrap-->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
