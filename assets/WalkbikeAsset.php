<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WalkbikeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.min.css',
        'https://fonts.googleapis.com/css?family=Open+Sans',
        'css/main.css',
    ];
    public $js = [
        'js/initmap.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyCvKMkI3bImm5vZdYC5Thsf851aQsLuxvc&callback=initMap'
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
