var map;
var markers = [];
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;

var max = 2;

var mapWindowCenter = {lat: 55.907718, lng: 36.443315};

function initMap() {

	map = new google.maps.Map(document.getElementById('mapCanvas'), {
		center: mapWindowCenter,
		zoom: 14,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		}
	});

	map.addListener('click', function (event) {
		addMarker(event.latLng);
	});

}
function createInputBlock() {
	var i = $('pointinput').length + 1;
	var inputblock = '<div class="row input-block pointinput"><div class="col address_block"><input type="text" name="marker' + i +
		'title" placeholder="Адрес или точка на карте" class="input-block_style __mod"><div class="abc green"><span>b</span></div></div><div class="col-r">' +
		'<select name="#" class="input-block_style starttransport"><option selected disabled>Выберите транспорт</option><option value="#">Велосипед</option>' +
		'<option value="#">Горный велосипед</option></select></div></div>';
	$('.pointinput').last().after(inputblock);
	max++;
}

function removeInputBlock() {
	if($('.pointinput').length > 2){
		$('.pointinput').last().remove();
		removeMarker();
		max--;
		labelIndex--;
	}
}

function addMarker(location) {
	if(markers.length < max){
		var marker = new google.maps.Marker({
			position: location,
			draggable: true,
			label: labels[labelIndex++ % labels.length],
			map: map
		});
		markers.push(marker);
	}
}
function removeMarker() {
	var i = markers.length - 1;
	markers[i].setMap(null);
	markers.pop();
}

function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

function clearMarkers() {
	setMapOnAll(null);
}

function showMarkers() {
	setMapOnAll(map);
}

function deleteMarkers() {
	labelIndex = 0;
	clearMarkers();
	markers = [];
}


// var walkBikeMarkers = [
//     {
//         position: { lat: 55.907718, lng: 36.443315 },
//         title: 'Старт маршрута',
//         content: 'Описание старта маршрута'
//     },
//     {
//         position: { lat: 55.903408, lng: 36.443105 },
//         title: 'Точка маршрута',
//         content: 'Описание промежуточной точки маршрута'
//     }
// ];
