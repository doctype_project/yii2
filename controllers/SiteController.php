<?php

namespace app\controllers;

use app\models\AddTravelForm;
use app\models\Tags;
use app\models\Travels;
use app\models\Vehicle;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $addTravelForm = new AddTravelForm();

        if ($addTravelForm->load(Yii::$app->request->post()) && $addTravelForm->validate()) {
            //Get Id last insert Travel
            $travel_id = $addTravelForm->addTravel();
            if ($travel_id) {
                //save points
                if ($addTravelForm->addPoint($travel_id)) {
                    if ($addTravelForm->addTag($travel_id)) {
                        Yii::$app->session->setFlash('success', "Новое путешествие создано!");
                    }
                }
            }
            else
            {
                Yii::$app->session->setFlash('error', "Введите название поездки!");
            }
            return $this->goHome();
        }
        else
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Travels::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
//            var_dump($dataProvider);
//            die();
            $vehicles = Vehicle::find()->select(['title', 'id'])->indexBy('id')->orderBy('id')->column();
            $tags = Tags::find()->select(['title', 'id'])->indexBy('id')->orderBy('id')->column();
            return $this->render('index', [
                'tags' => $tags,
                'vehicles' => $vehicles,
                'addTravelForm' => $addTravelForm,
                'dataProvider' => $dataProvider
            ]);
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
